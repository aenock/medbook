'use strict';

// Register `phoneList` component, along with its associated controller and template
angular.
    module('menuList').
    component('menuList', {
        templateUrl: 'menu-list/menu-list.template.html',
        controller: ['$scope',
            function MenuListController($scope) {
                // this.phones = Phone.query();
                var self = this;
                this.menuItems = [
                    {
                        name: 'Salad', choices: [
                            { name: 'Santa Fe' },
                            { name: 'Greek' },
                            { name: 'Asian' },
                        ],
                        related: [
                            {
                                name: 'Dressing', choices: [
                                    { name: 'Italian' },
                                    { name: 'Blue Cheese' },
                                    { name: 'Ranch' },
                                ]
                            },
                            {
                                name: 'Bread', choices: [
                                    { name: 'Italian' },
                                    { name: 'Flat' },
                                    { name: 'Sourdough' },
                                ]
                            }
                        ]
                    },
                    {
                        name: 'Entree', choices: [
                            { name: 'Steak' },
                            { name: 'Salmon' },
                            { name: 'Rice' },
                        ],
                        related: [
                        ]
                    },
                    {
                        name: 'Soup', choices: [
                            { name: 'Minestrone' },
                            { name: 'Hot and sour' },
                            { name: 'Miso' },
                        ],
                        related: [
                            {
                                name: 'Bread', choices: [
                                    { name: 'Breadsticks' }
                                ]
                            }
                        ]
                    }
                ];

                this.orderProp = 'age';

                $scope.updateSelection = function (item) {
                    console.log($scope.selectedItems);
                }

                $scope.itemIndex;

                $scope.selectedItems = [];
                $scope.selectedChoices = [];
                $scope.selectedRelated = [];

                $scope.isAnyItemSelected = function () {
                    for (var i = 0; i < this.selectedItems.length; i++) {
                        if (this.selectedItems[i] == false) {
                            console.log(this.selectedItems[i], "true");
                            return true;
                        }
                    }
                    console.log("false");
                    return false;
                }

                for (var i = 0; i < this.menuItems; i++) {
                    this.menuItems[i].selected = false;
                }

                $scope.changedParent = function (index) {
                    console.log("parentitem, ", self.menuItems[index])
                }

                self.showOptionsInParentWithIndex = [];


                $scope.changedChoice = function (parentIndex, index) {
                    self.showOptionsInParentWithIndex[parentIndex] = false;
                    for (var i = 0; i < self.menuItems[parentIndex].choices.length; i++) {
                        if (self.menuItems[parentIndex].choices[i].selected) {
                            self.showOptionsInParentWithIndex[parentIndex] = true;
                        }
                    }
                }
                
                self.showOptionsInChoiceWithParentChoiceIndex = [];
                for(var i = 0; i < self.menuItems.length; i++){
                    for(var j = 0; j < self.menuItems.length; j++){
                        self.showOptionsInChoiceWithParentChoiceIndex[i] = [];
                    }
                }
                

                $scope.changedRelatedChoice = function (parentIndex, choiceIndex, relatedChoice) {
                    self.showOptionsInChoiceWithParentChoiceIndex[parentIndex][choiceIndex] = false;
                    if (relatedChoice.selected) {
                        self.showOptionsInChoiceWithParentChoiceIndex[parentIndex][choiceIndex] = true;
                    }
                }

            }
        ]
    });
