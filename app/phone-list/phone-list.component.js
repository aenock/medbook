'use strict';

// Register `phoneList` component, along with its associated controller and template
angular.
  module('phoneList').
  component('phoneList', {
    templateUrl: 'phone-list/phone-list.template.html',
    controller: ['Phone', '$scope',
      function PhoneListController(Phone, $scope) {
        this.phones = Phone.query();
        this.menuItems = [
          {
            name: 'Salad', choices: [
              { name: 'Santa Fe' },
              { name: 'Greek' },
              { name: 'Asian' },
            ],
            related: [
              {
                name: 'Dressing', choices: [
                  { name: 'Italian' },
                  { name: 'Blue Cheese' },
                  { name: 'Ranch' },
                ]
              },
              {
                name: 'Bread', choices: [
                  { name: 'Italian' },
                  { name: 'Flat' },
                  { name: 'Sourdough' },
                ]
              }
            ]
          },
          {
            name: 'Entree', choices: [
              { name: 'Steak' },
              { name: 'Salmon' },
              { name: 'Rice' },
            ],
            related: [
            ]
          },
          {
            name: 'Soup', choices: [
              { name: 'Minestrone' },
              { name: 'Hot and sour' },
              { name: 'Miso' },
            ],
            related: [
              {
                name: 'Bread', choices: [
                  { name: 'Breadsticks' }
                ]
              }
            ]
          }
        ];

        this.orderProp = 'age';

        $scope.updateSelection = function(item){
          console.log($scope.selectedItems);
        }

        $scope.selectedItems = [];
        $scope.selectedChoices = [];
        $scope.selectedRelated = [];
        // for(var i = 0; i < this.menuItems.length; i++){
        //   this.menuItems.selectedItems[i] = false;
        //   for(var i = 0; i < this.menuItems.length; i++){
        //     this.menuItems.selectedChoices[i] = false;
        //     $scope.selectedChoices = [];
        //     for(var i = 0; i < this.menuItems.length; i++){
        //       this.menuItems.selectedRelated[i] = false;
        //     }
        //   }
        // }
        
        
        
        
        
        
      }
    ]
  });
