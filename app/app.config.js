'use strict';

angular.
  module('phonecatApp').
  config(['$routeProvider',
    function config($routeProvider) {
      $routeProvider.
        when('/phones', {
          template: '<phone-list></phone-list>'
        }).
        when('/menu-list', {
          template: '<menu-list></menu-list>'
        }).
        otherwise('/menu-list');
    }
  ]);
